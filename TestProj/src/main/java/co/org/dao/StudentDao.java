package co.org.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

@Component
public class StudentDao {

	private JdbcTemplate jdbc;
	
	@Autowired
	public void setDataSource(DataSource datasource){
		this.jdbc = new JdbcTemplate(datasource);
	}
	public List<Student> getStu(){
		return jdbc.query("select *from tbl_stu",new RowMapper<Student>() {
			@Override
			public Student mapRow(ResultSet rs, int rowno) throws SQLException {
				Student s = new Student();
				s.setId(rs.getInt("id"));
				s.setName(rs.getString("name"));
				s.setAge(rs.getString("age"));
				return s;
			}
		});
	
		}
	
}
