package co.org.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.org.dao.Student;
import co.org.dao.StudentDao;

@Service
public class StudentService {

	//Here autowired happens via constructor [default]
	@Autowired
	private StudentDao studao;
	
	
	public List<Student> getStu(){
		return studao.getStu();
	}
	
	public String dummy(int id) throws Exception{
		System.out.println("Service Method");
		//throw new Exception("bbb");
		return "vanathi";
	}
	public void dummy1() throws Exception{
		System.out.println("Service Method");
		//throw new Exception("bbb");
	}
}
